# CVC64 Project Template

Tired of installing ~100GB's of proprietary garbage just to simulate a 
simple verilog module? Then this is the tool for YOU!

Compile your verilog code in a few seconds directly to a native 
binary that runs BLAZINGLY FAST!1!. After this view the signals
using `gtkwave`, that loads in seconds instead of minutes.

# Setup (Archlinux based distribution)
If you are using an AUR helper like `yay` you can install CVC by running
the following command: 
``` shell
yay -S oss-cvc-git
```
You also need `gtkwave` which can be installed from pacman.

# Setup (other distributions)
The repo provides the `container_run.sh` script, which builds a suitable
container with the required tools, and basically forwards the arguments
to the `make` command inside the container.

Running `container_run.sh simulate` basically runs `make simulate` inside the
container.

You also need to install `gtkwave` from a package manager/other source.

# Usage
Write your source code files and edit the simulate rule inside 
the makefile to compile your sources. Run 
`make simulate`/`container_run.sh simulate`.
Note that only test fixtures can be compiled and run.

Then run `make gtkwave` to open the wave file.
The wave view inside gtkwave does not update automatically if the file
on disk is changed, but it can be reloaded manually by pressing `Control+Shift+R`.
