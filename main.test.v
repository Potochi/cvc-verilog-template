// Using includes is recommended, as you can pass
// single files to the compiler, simplifying the
// build step.
// Not using these requires passing all needed files
// to the compiler manually
`include "./main.v"

// Set the simulation timescale
`timescale 1 ns/10 ps

// Test fixture module
module TesterModule;
   reg in;
   wire out;

   localparam period = 20;

   // Instantiate the module you want to test
   MainModule main(.in(in), .out(out));

   // This initial block will be executed only once
   initial begin
      // A change in inputs causes a change in outupts.
      // This can be viewed with gtkwave
      in = 0;
      #period;

      in = 1;
      #period;

      in = 0;
      #period;

      // Sometimes CVC cuts the last state from the fstvars file,
      // You can add a bogus last state to force the previous ones
      // to appear.
      //
      // Example bogus state:
      // ```verilog
      // #1000;
      // in = 1;
      // ```
   end
endmodule
