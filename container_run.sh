#!/bin/sh

# Export our user's id and gid to map it to root's
# uid and gid inside the container. This makes files
# created inside the container by root, be actually
# owned by our user outside the container
export DOCKER_UID=$(id -u)
export DOCKER_GID=$(id -u)

docker compose run cvc $@
